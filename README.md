# Letter Counts

## Name
Letter Counts Cargo Lambda Tool

## Description
This is a cargo lambda tool that takes in a json in the format {"input string": "any text"}, and it returns the letter counts.

## Example Input

#### Input Test into Lambda Function:

![alt text](images/image-1.png)

#### Output from Test:

![alt text](images/image-2.png)

#### Input from terminal, input.json:

![Alt text](images/image-4.png)

#### Output using the terminal:

![Alt text](images/image-3.png)

## Installation

For cargo lambda, refer to the [website](https://www.cargo-lambda.info/guide/what-is-cargo-lambda.html).

```
cargo lambda new lettercounter
cd lettercounter
cargo lambda watch
cargo lambda build --release
cargo lambda invoke --data-file input.json 
cargo lambda deploy
cargo lambda invoke --remote lettercounter --data-file input.json # alternative
```

The Makefile will run all the necessary commands, with make all.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Steps to Setup on AWS Cloud 9
    
    1. Make a new cargo lambda project.
        a) Install brew via sudo passwd ubuntu, as ubuntu will need a password to install brew.
        b) Install Rust and Cargo Lambda.
        
    2. Ensure that your ports for the "watch" command are free (solutions available online).

    3. Install awscli and configure credentials.
        a) You can also run these commands in the terminal to circumvent much usage of the CLI. (Recommended)
            i) export AWS_ACCESS_KEY_ID=<access_key>
            ii) export AWS_SECRET_ACCESS_KEY=<secret_key>
            iii) export AWS_REGION=<region_name>
            
        b) You can also add credentials manually to the AWS Cloud 9 instance's profile or by connecting directly to an IAM user.
        
        c) Use the AWS cli to configure credentials.

    4. Build and deploy the cargo lambda, thanks to your credentials.

    5. Optional :  Make a Makefile to simplify use of repetitive commands.

## Adding Logging to your Rust Scripts : 1. Tracing!!!

    async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract the input string from the request

    info!("Received a payload!!!"); # This is logging!!!

    let input_string = event.payload.input_string;

    // Calculate letter frequency
    let letter_frequency = calculate_letter_frequency(&input_string);

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        msg: format!("Letter frequency: {:?}", letter_frequency),
    };

    info!("Processed the payload : {} !!!", input_string); # This is logging!!!

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
    }

## Add logging on your AWS Lambda via Xray and Cloudwatch

    1. Turn on AWS X-Ray

    2. Find CloudWatch for your lambda function and look at the audit trails.

![alt text](images/image-6.png)

![alt text](images/image-7.png)

## Authors and acknowledgment
Big thanks to the cloud computing coursera course, from Noah Gift. 

## License
Creative Commons

## Mini Project Details

This submission counts for Mini Project 2 and 6.

## Project status
This project has been completed, as of February 2024. (Mini Project 2)

The logging has been completed, as of April 2024. (Mini Project 6)

